public class Enemy extends Character {
    public Enemy(int maxHealth, String name, int attack, int speed) {
        super(maxHealth, name, attack, speed);
    }

    @Override
    public void move() {
        System.out.println(getName() + "moved");
    }

    @Override
    public void description() {
        System.out.println(getName() + " " + "HP " + getMaxHealth());
        System.out.println("ATK " + getAttack() + " " + "SPD " + getSpeed());
        System.out.println("--------------");
    }
}
