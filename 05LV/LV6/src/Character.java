public abstract class Character {
    private int maxHealth;
    private String name;
    private int attack;
    private int speed;

    public Character(int maxHealth, String name, int attack, int speed) {
        setMaxHealth(maxHealth);
        setName(name);
        setAttack(attack);
        setSpeed(speed);
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    abstract public void move();

    abstract public void description();
}
