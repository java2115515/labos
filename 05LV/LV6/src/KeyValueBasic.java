public class KeyValueBasic {
    private final int key;
    private String value;

    public KeyValueBasic(int key, String value) {
        this.key = key;
        setValue(value);
    }

    public static void main(String[] args) {
        KeyValueBasic kv = new KeyValueBasic(42, "foo");
        System.out.println(kv);
        kv.setValue("bar");
        System.out.println(kv);
    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return getKey() + ": " + getValue();
    }

}
