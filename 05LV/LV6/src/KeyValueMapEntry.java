import java.util.HashMap;
import java.util.Map;

public class KeyValueMapEntry<K, V> implements Map.Entry<K, V> {
    private final K key;
    private V value;

    public KeyValueMapEntry(K key, V value) {
        this.key = key;
        setValue(value);
    }

    public static void main(String[] args) {
        // Create a HashMap to store KeyValueMapEntry objects
        HashMap<Integer, String> map = new HashMap<>();

        // Add some KeyValueMapEntry objects to the map
        KeyValueMapEntry<Integer, String> entry1 = new KeyValueMapEntry<>(1, "apple");
        KeyValueMapEntry<Integer, String> entry2 = new KeyValueMapEntry<>(2, "banana");
        KeyValueMapEntry<Integer, String> entry3 = new KeyValueMapEntry<>(3, "orange");
        map.put(entry1.getKey(), entry1.getValue());
        map.put(entry2.getKey(), entry2.getValue());
        map.put(entry3.getKey(), entry3.getValue());

        // Iterate over the map using a for-each loop and print out the key-value pairs
        for (HashMap.Entry<Integer, String> entry : map.entrySet()) {
            System.out.println("Key: " + entry.getKey() + ", Value: " + entry.getValue());
        }
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    public V setValue(V value) {
        V oldValue = this.value;
        this.value = value;
        return oldValue;
    }
}
