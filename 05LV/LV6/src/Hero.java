public class Hero extends Character implements iDrinkable {
    private final int speedModifier = 3;
    private final int healthModifier = 10;
    private final int attackModifier = 5;

    public Hero(int maxHealth, String name, int attack, int speed) {
        super(maxHealth, name, attack, speed);
    }

    public void drinkHealthPotion() {
        setMaxHealth(getMaxHealth() + healthModifier);
        System.out.println(getName() + " drank a health potion (HP+" + healthModifier + ")");
        description();
    }

    public void drinkAttackPotion() {
        setAttack(getAttack() + attackModifier);
        System.out.println(getName() + " drank an attack potion (ATK+" + attackModifier + ")");
        description();
    }

    public void drinkSpeedPotion() {
        setSpeed(getSpeed() + speedModifier);
        System.out.println(getName() + " drank a speed potion (SPD+" + speedModifier + ")");
        description();
    }

    @Override
    public void move() {
        System.out.println(getName() + "moved");
    }

    @Override
    public void description() {
        System.out.println(getName() + " " + "HP " + getMaxHealth());
        System.out.println("ATK " + getAttack() + " " + "SPD " + getSpeed());
        System.out.println("--------------");
    }
}
