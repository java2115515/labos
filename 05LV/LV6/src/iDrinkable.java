public interface iDrinkable {
    void drinkSpeedPotion();

    void drinkHealthPotion();

    void drinkAttackPotion();
}
