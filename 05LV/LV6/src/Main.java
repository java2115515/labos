import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Hero hero = new Hero(20, "King", 10, 5);
        hero.description();
        Enemy enemy1 = new Enemy(15, "Bandit", 6, 15);
        enemy1.description();

        Scanner scanner = new Scanner(System.in);
        System.out.println("Which potion do you want to drink? (1: Health, 2: Attack, 3: Speed)");
        int potionChoice = scanner.nextInt();

        switch (potionChoice) {
            case 1:
                hero.drinkHealthPotion();
                break;
            case 2:
                hero.drinkAttackPotion();
                break;
            case 3:
                hero.drinkSpeedPotion();
                break;
            default:
                System.out.println("Invalid potion choice");
        }
        if (hero.getAttack() > enemy1.getAttack()) {
            System.out.println(hero.getName() + " has won");
            System.out.println("--------------");
        } else {
            System.out.println(enemy1.getName() + " has won");
            System.out.println("--------------");
        }

        Enemy boss = new Enemy(200, "Chief", 50, 50) {
            @Override
            public void description() {
                System.out.println("BOSS!");
                System.out.println(getName() + " " + "HP " + getMaxHealth());
                System.out.println("ATK " + getAttack() + " " + "SPD " + getSpeed());
                System.out.println("--------------");
            }
        };
        boss.description();
        Behaviour uniqueCreature = new Behaviour() {
            private final int maxHealth = 100;
            private final int attack = 15;
            private final int speed = 20;

            @Override
            public void move() {
                System.out.println("The unique creature moved");
            }

            @Override
            public void description() {
                System.out.println("The unique creature: " + "HP " + maxHealth);
                System.out.println("ATK " + attack + " " + "SPD " + speed);
            }
        };
        uniqueCreature.description();
        uniqueCreature.move();
    }
}


