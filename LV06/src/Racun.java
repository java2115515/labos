import java.util.ArrayList;
import java.util.Date;

public class Racun implements UpravljanjeRacunima {
    private final ArrayList<Artikl> listaArtikala;
    private final Date datumIzdavanja;
    private double ukupnaCijena;
    private Konobar konobar;

    public Racun(Konobar konobar) {
        listaArtikala = new ArrayList<>();
        datumIzdavanja = new Date();
        setKonobar(konobar);

    }

    public void dodajArtikl(Artikl artikl) {
        listaArtikala.add(artikl);
        ukupnaCijena += artikl.getCijena();
        System.out.println("Dodan artikl: " + artikl.getNaziv() + " +" + artikl.getCijena() + "€");
    }

    public void izbaciArtikl(Artikl artikl) {
        if (listaArtikala.remove(artikl)) {
            ukupnaCijena -= artikl.getCijena();
            System.out.println("Izbacen artikl: " + artikl.getNaziv() + " -" + artikl.getCijena() + "€");
        } else {
            System.out.println("Artikl ne postoji na računu.");
        }
    }

    @Override
    public String toString() {
        String outputString = "";
        outputString += datumIzdavanja;
        outputString += "\n";
        for (Artikl artikl : listaArtikala) {
            outputString += artikl.toString();
            outputString += "€\n";
        }
        outputString += izracunajUkupnuCijenu();
        outputString += "€";
        return outputString;
    }

    public double izracunajUkupnuCijenu() {
        return ukupnaCijena;
    }

    public double getUkupnaCijena() {
        return ukupnaCijena;
    }

    public void setUkupnaCijena(double ukupnaCijena) {
        this.ukupnaCijena = ukupnaCijena;
    }

    public Date getDatumIzdavanja() {
        return datumIzdavanja;
    }

    public Konobar getKonobar() {
        return konobar;
    }

    public void setKonobar(Konobar konobar) {
        this.konobar = konobar;
    }
}
