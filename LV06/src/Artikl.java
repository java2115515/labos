public class Artikl {
    private String naziv;
    private double cijena;
    private int barcode;

    public Artikl(int barcode, String naziv, double cijena) {
        setBarcode(barcode);
        setNaziv(naziv);
        setCijena(cijena);
    }

    public Artikl(Artikl artikl) {
        setBarcode(artikl.barcode);
        setNaziv(artikl.naziv);
        setCijena(artikl.cijena);
    }

    @Override
    public String toString() {
        return getNaziv() + " " + getCijena();
    }

    public int getBarcode() {
        return barcode;
    }

    public void setBarcode(int barcode) {
        this.barcode = barcode;
    }

    public double getCijena() {
        return cijena;
    }

    public void setCijena(double cijena) {
        this.cijena = cijena;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }
}
