public class Jelo extends Artikl {
    private String opis;

    public Jelo(int barcode, String naziv, double cijena, String opis) {
        super(barcode, naziv, cijena);
        setOpis(opis);
    }

    public Jelo(Jelo jelo) {
        super(jelo.getBarcode(), jelo.getNaziv(), jelo.getCijena());
        this.opis = jelo.opis;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    @Override
    public String toString() {
        return super.toString() + " " + getOpis();
    }
}
