public class Konobar {
    private String ime;
    private String prezime;
    private int brojNarudzbi;

    public Konobar(String ime, String prezime) {
        setIme(ime);
        setPrezime(prezime);
        setBrojNarudzbi(0);
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public int getBrojNarudzbi() {
        return brojNarudzbi;
    }

    public void setBrojNarudzbi(int brojNarudzbi) {
        this.brojNarudzbi = brojNarudzbi;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }
}
