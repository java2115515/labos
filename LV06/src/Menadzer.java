public class Menadzer {
    private Kafic kafic;
    private String korisnickoIme;
    private String lozinka;

    public Menadzer(String korisnickoIme, String lozinka) {
        setKorisnickoIme(korisnickoIme);
        setLozinka(lozinka);
        setKafic(null);
    }

    public Kafic prijava(String korisnickoIme, String lozinka) {
        if (korisnickoIme.equals(this.korisnickoIme) && lozinka.equals(this.lozinka)) {
            setKafic(new Kafic());
            return this.kafic;
        }
        System.out.println("Greška: ime ili lozinka nisu točni.");
        return null;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public Kafic getKafic() {
        return kafic;
    }

    public void setKafic(Kafic kafic) {
        this.kafic = kafic;
    }
}
