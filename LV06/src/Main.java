import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Menadzer menadzer = new Menadzer("user", "password");
        Kafic kafic = null;
        Konobar konobar = null;

        // pre definirani artikli
        Artikl jelo0 = new Jelo(4312, "Cheeseburger", 4, "Burger");
        Artikl jelo1 = new Jelo(4313, "Classic Burger", 3.5, "Burger");
        Artikl jelo2 = new Jelo(3312, "Pizza Pikante Jambo", 10, "Pizza");
        Artikl pice0 = new Pice(332, "Coca-cola", 3, "Gazirano pice");
        Artikl pice1 = new Pice(232, "Espresso L", 1.5, "Topli napitak");
        Artikl pice2 = new Pice(832, "Heineken", 2.5, "Alkoholno pice");

        ArrayList<Artikl> ponudeniArtikli = new ArrayList<>();
        ponudeniArtikli.add(jelo0);
        ponudeniArtikli.add(jelo1);
        ponudeniArtikli.add(jelo2);
        ponudeniArtikli.add(pice0);
        ponudeniArtikli.add(pice1);
        ponudeniArtikli.add(pice2);

        // ordered articles lista
        ArrayList<Artikl> artikliNaLageru = new ArrayList<>();

        // selection menu

        while (menadzer.getKafic() == null) {
            System.out.println("Unesite korisničko ime: ");
            String username = scanner.nextLine();
            System.out.println("Unesite lozinku: ");
            String password = scanner.nextLine();

            kafic = menadzer.prijava(username, password);
            konobar = new Konobar("Pero", "Perić");
        }

        System.out.println("Uspješna prijava! Dobrodošli, " + menadzer.getKorisnickoIme() + ".");

        while (true) {
            System.out.println("Konobar: " + konobar.getIme() + " " + konobar.getPrezime());
            System.out.println("Ukupna zarada: " + kafic.izracunajUkupnuCijenu() + "€");
            System.out.println("\nOdaberite opciju:");
            System.out.println("1. Unos naručenog artikla");
            System.out.println("2. Izbacivanje artikla");
            System.out.println("3. Novi račun");
            System.out.println("4. Ispis svih računa");
            System.out.println("5. Izlaz");

            int option = scanner.nextInt();
            scanner.nextLine(); // consume newline character

            switch (option) {
                case 1:
                    System.out.println("Unesite barkod artikla: ");
                    int barcode = scanner.nextInt();
                    scanner.nextLine(); // consume newline character

                    // Find the article with the given barcode
                    Artikl articleToAdd = null;
                    for (Artikl a : ponudeniArtikli) {
                        if (a.getBarcode() == barcode) {
                            articleToAdd = a;
                            break;
                        }
                    }

                    // Add the article to the list of ordered articles
                    if (articleToAdd != null) {
                        artikliNaLageru.add(articleToAdd);
                        System.out.println("Artikl \"" + articleToAdd.getNaziv() + "\" dodan u skladište.");
                    } else {
                        System.out.println("Greška: artikl s unesenim barkodom nije pronađen.");
                    }
                    break;
                case 2:
                    System.out.println("Unesite barkod artikla koji želite izbaciti: ");
                    int barcodeToRemove = scanner.nextInt();
                    scanner.nextLine(); // consume newline character

                    // Find the article with the given barcode
                    Artikl articleToRemove = null;
                    for (Artikl a : artikliNaLageru) {
                        if (a.getBarcode() == barcodeToRemove) {
                            articleToRemove = a;
                            break;
                        }
                    }

                    // Remove the article from the list of ordered articles
                    if (articleToRemove != null) {
                        artikliNaLageru.remove(articleToRemove);
                        System.out.println("Artikl \"" + articleToRemove.getNaziv() + "\" izbačen iz skladišta.");
                    } else {
                        System.out.println("Greška: artikl s unesenim barkodom nije pronađen.");
                    }
                    break;
                case 3:
                    Racun racun = menadzer.getKafic().kreirajRacun(konobar);

                    // Add copies of the articles in the artikliNaLageru list to the Racun
                    while (true) {
                        System.out.println("Unesite barkod artikla (0 za kraj): ");
                        barcode = scanner.nextInt();
                        scanner.nextLine(); // consume newline character

                        if (barcode == 0) {
                            break;
                        }

                        // Find the article with the given barcode in the artikliNaLageru list
                        articleToAdd = null;
                        for (Artikl a : artikliNaLageru) {
                            if (a.getBarcode() == barcode) {
                                articleToAdd = a;
                                break;
                            }
                        }

                        // Add a copy of the article to the Racun
                        if (articleToAdd != null) {
                            racun.dodajArtikl(new Artikl(articleToAdd));
                            System.out.println("Artikl \"" + articleToAdd.getNaziv() + "\" dodan u račun.");
                        } else {
                            System.out.println("Greška: artikl s unesenim barkodom nije pronađen u skladištu.");
                        }
                    }

                    // Print the Racun
                    System.out.println("\nRačun:");
                    System.out.println(racun.toString());
                    break;
                case 4:
                    System.out.println("--------------------------------");
                    System.out.println(menadzer.getKafic().ispisSvihRacuna());
                    System.out.println("--------------------------------");

                    break;
                case 5:
                    System.out.println("Izlaz iz programa...");
                    System.exit(0);
                default:
                    System.out.println("Krivi unos, molimo pokušajte ponovno.");
                    break;
            }
        }
    }
}
