public class Pice extends Artikl {
    private String vrsta;

    public Pice(int barcode, String naziv, double cijena, String vrsta) {
        super(barcode, naziv, cijena);
        setVrsta(vrsta);
    }

    public Pice(Pice pice) {
        super(pice.getBarcode(), pice.getNaziv(), pice.getCijena());
        this.vrsta = pice.vrsta;
    }

    @Override
    public String toString() {
        return super.toString() + " " + getVrsta();
    }

    public String getVrsta() {
        return vrsta;
    }

    public void setVrsta(String vrsta) {
        this.vrsta = vrsta;
    }
}
