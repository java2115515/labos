import java.util.HashMap;
import java.util.LinkedList;

public class Kafic implements UpravljanjeRacunima {
    private final LinkedList<Racun> listaRacuna;
    private final HashMap<Integer, Artikl> listaArtikala;
    private double ukupnaZarada;

    public Kafic() {
        listaArtikala = new HashMap<>();
        listaRacuna = new LinkedList<>();
        setUkupnaZarada(0);
    }

    public Racun kreirajRacun(Konobar konobar) {
        Racun racun = new Racun(konobar);
        listaRacuna.add(racun);
        return racun;
    }

    public void dodajArtikl(Artikl artikl) {
        listaArtikala.put(artikl.getBarcode(), artikl);
        System.out.println("Dodan artikl: " + artikl.getNaziv() + " +" + artikl.getCijena() + "€");
    }

    public void izbaciArtikl(Artikl artikl) {
        if (listaArtikala.remove(artikl.getBarcode(), artikl)) {
            System.out.println("Izbacen artikl: " + artikl.getNaziv() + " -" + artikl.getCijena() + "€");
        } else {
            System.out.println("Artikl ne postoji na računu.");
        }
    }

    public double izracunajUkupnuCijenu() {
        setUkupnaZarada(0);
        for (Racun racun : listaRacuna) {
            setUkupnaZarada(getUkupnaZarada() + racun.izracunajUkupnuCijenu());
        }
        return ukupnaZarada;
    }

    public String ispisSvihRacuna() {
        if (!listaRacuna.isEmpty()) {
            String outputString = "";
            for (Racun racun : listaRacuna) {
                outputString = racun.toString();
            }
            return outputString;
        }
        return "Nema prijašnjih računa.";
    }

    public double getUkupnaZarada() {
        return ukupnaZarada;
    }

    public void setUkupnaZarada(double ukupnaZarada) {
        this.ukupnaZarada = ukupnaZarada;
    }
}
